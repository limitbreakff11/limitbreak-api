const jwt = require('jsonwebtoken');
const request = require('request');
const { Router } = require('express');
const axios = require('axios');

const router = Router();

const argon2 = require('argon2-browser');

const {
  formatAvatar,
  formatJobString,
  titleIdToString,
} = require('./utils/chars');
const {
  generateHash,
  generateSalt,
  getJWTForAccountId,
  uuid,
} = require('./utils/accounts');

const mailgun = require('mailgun-js');
const mg = mailgun({
  apiKey: process.env.MAILGUN_API_KEY,
  domain: process.env.MAILGUN_DOMAIN,
});

const validate = (req, res, next) => {
  const token = req.headers.authorization.replace(/^Bearer\s/, '');
  jwt.verify(token, process.env.JWT_SECRET, (error, decoded) => {
    if (!error) {
      req.jwt = decoded;
      next();
    } else {
      res.status(401).send();
    }
  });
};

router.get('/profile', validate, async (req, res) => {
  try {
    const statement = `SELECT *, IF(accounts_sessions.charid IS NULL, 0, 1) AS \`online\` FROM chars
            JOIN char_stats ON chars.charid = char_stats.charid
            JOIN char_look ON chars.charid = char_look.charid
            LEFT JOIN accounts_sessions on chars.charid = accounts_sessions.charid
            WHERE chars.accid = ?`;

    const results = await req.app.locals.query(statement, [req.jwt.id]);

    const chars = results.map(char => ({
      name: char.charname,
      online: char.online,
      title: titleIdToString(char.title),
      job: formatJobString(char),
      avatar: formatAvatar(char),
    }));
    const token = await getJWTForAccountId(
      req.app.locals.loginQuery,
      req.jwt.id
    );
    const {
      id,
      username,
      email,
      timecreated,
      timemodified,
      status,
    } = jwt.decode(token);
    res.send({
      jwt: token,
      profile: Object.assign(
        { id, username, email, timecreated, timemodified, status },
        { chars }
      ),
    });
  } catch (error) {
    console.log(error);
    res.status(500).send();
  }
});

router.post('/verify', async (req, res) => {
  const { code } = req.query;

  const statement = `
    UPDATE accounts
    INNER JOIN account_modification_codes ON account_modification_codes.account_id = accounts.id
    SET status = 1
    WHERE verification_code = ?
  ;`;

  const results = await req.app.locals.loginQuery(statement, [code]);
  if (results.affectedRows) {
    res.sendStatus(200);
  } else {
    res.sendStatus(400);
  }
});

router.post('/register', async (req, res) => {
  const disallowedIP = [];

  const {
    username,
    password,
    email,
    confirmUsername,
    confirmPassword,
    confirmEmail,
    recaptcha,
  } = req.body;

  /** common origins attemping to register
   * https://www.edenxi.com/tools
   * https://edenxi.com/tools
   * http://www.edenxi.com/tools
   * http://play.edenxi.com/tools
   */
  try {
    const tests = [
      !disallowedIP.includes(req.headers['x-forwarded-for']),
      password === confirmPassword,
      username.toLowerCase() === confirmUsername.toLowerCase(),
      email.toLowerCase() === confirmEmail.toLowerCase(),
      username.length >= 4,
      password.length >= 6 && password.length <= 25,
      !!username,
      !!recaptcha,
    ];

    if (!!tests.includes(false)) {
      res.json({
        status: 'ERROR',
        errors: {
          server: 'Make sure you filled in all the fields correctly.',
          client: {
            username: !(tests[2] && tests[4] && tests[6]),
            password: !tests[5],
            confirmPassword: !tests[1],
            email: !tests[3],
            recaptcha: !tests[7],
          },
        },
      });
    } else {
      // All pre-tests have passed. Validate the Recaptcha3.
      const {
        data: { success: recaptchaSuccess },
      } = await axios.post(
        `https://www.google.com/recaptcha/api/siteverify?secret=${process.env.RECAPTCHA_SECRET}&response=${recaptcha}&remoteip=${req.headers['x-forwarded-for']}`
      );

      if (!recaptchaSuccess) {
        return res.json({
          status: 'ERROR',
          errors: {
            server: 'Error with captcha validation.',
          },
        });
      }

      try {
        const checkEmailStatement = 'SELECT * FROM accounts WHERE `email` = ?';
        const checkEmailResults = await req.app.locals.loginQuery(
          checkEmailStatement,
          [email]
        );

        if (checkEmailResults.length > 0) {
          return res.json({
            status: 'ERROR',
            errors: {
              server: 'Email already taken. Try something else.',
            },
          });
        }

        const salt = generateSalt();
        const passwordHash = generateHash(password, salt);

        // attempt to register the account
        const statement =
          'INSERT INTO accounts (`username`,`password`,`salt`,`email`) VALUES (?, ?, ?, ?);';
        const results = await req.app.locals.loginQuery(statement, [
          username,
          passwordHash,
          salt,
          email,
        ]);

        try {
          const phpBBStatement =
            "INSERT INTO `phpbb_users` (`user_type`, `group_id`, `user_permissions`, `user_perm_from`, `user_ip`, `user_regdate`, `username`, `username_clean`, `user_password`, `user_passchg`, `user_email`, `user_birthday`, `user_lastvisit`, `user_lastmark`, `user_lastpost_time`, `user_lastpage`, `user_last_confirm_key`, `user_last_search`, `user_warnings`, `user_last_warning`, `user_login_attempts`, `user_inactive_reason`, `user_inactive_time`, `user_posts`, `user_lang`, `user_timezone`, `user_dateformat`, `user_style`, `user_rank`, `user_colour`, `user_new_privmsg`, `user_unread_privmsg`, `user_last_privmsg`, `user_message_rules`, `user_full_folder`, `user_emailtime`, `user_topic_show_days`, `user_topic_sortby_type`, `user_topic_sortby_dir`, `user_post_show_days`, `user_post_sortby_type`, `user_post_sortby_dir`, `user_notify`, `user_notify_pm`, `user_notify_type`, `user_allow_pm`, `user_allow_viewonline`, `user_allow_viewemail`, `user_allow_massemail`, `user_options`, `user_avatar`, `user_avatar_type`, `user_avatar_width`, `user_avatar_height`, `user_sig`, `user_sig_bbcode_uid`, `user_sig_bbcode_bitfield`, `user_jabber`, `user_actkey`, `reset_token`, `reset_token_expiration`, `user_newpasswd`, `user_form_salt`, `user_new`, `user_reminded`, `user_reminded_time`) VALUES ('0', '2', '00000000000v6ez2zu\nhwby9w000000\nm6awadqmx0qo', '0', '', ?, ?, ?, ?, '0', ?, '', '0', '0', '0', '', '', '0', '0', '0', '0', '0', '0', '0', '', '', 'D M d, Y g:i a', '0', '0', '', '0', '0', '0', '0', '-3', '0', '0', 't', 'd', '0', 't', 'a', '0', '1', '0', '1', '1', '1', '1', '230271', '', '', '0', '0', '<t></t>', '', '', '', '', '', '0', '', ?, '1', '0', '0');";

          // do the user_group
          const phpBBSalt = uuid().substring(0, 15);
          const phpBBHash = await argon2.hash({
            pass: password,
            salt: phpBBSalt,
            type: argon2.ArgonType.Argon2id,
          });
          await req.app.locals.forumQuery(phpBBStatement, [
            Math.floor(Date.now() / 1000),
            username,
            username.toLowerCase(),
            phpBBHash?.encoded,
            email,
            phpBBSalt,
          ]);
        } catch (e) {
          console.log(e);
        }

        if (results.affectedRows) {
          const infoStatement = `SELECT id FROM accounts WHERE username = ?;`;
          const info = await req.app.locals.loginQuery(infoStatement, [
            username,
          ]);

          // Add 3 entries into contents table
          for (let i = 0; i < parseInt(process.env.CONTENT_IDS); i++) {
            const contentStatement =
              'INSERT INTO contents (`account_id`, `enabled`) VALUES (?, 1)';

            req.app.locals.loginQuery(contentStatement, [info[0].id]);
          }

          const token = await getJWTForAccountId(
            req.app.locals.loginQuery,
            info[0].id
          );
          const {
            id,
            username: login,
            email,
            timecreated,
            timemodified,
            status,
          } = jwt.decode(token);

          const accountModificationCodesStatement =
            'INSERT INTO account_modification_codes (`account_id`, `verification_code`) VALUES (?, ?);';

          const verificationCode = uuid();

          await req.app.locals.loginQuery(accountModificationCodesStatement, [
            id,
            verificationCode,
          ]);

          const data = {
            from: `Limitbreak <noreply@${process.env.MAILGUN_DOMAIN}>`,
            to: email,
            subject: 'Confirm your Registration',
            html: `<p>Hi! Thanks for signing up to Limitbreak, here\'s your signup link: <a href="${process.env.SITE_URL}/confirm?code=${verificationCode}">${process.env.SITE_URL}/confirm?code=${verificationCode}</a>.</p>`,
          };

          mg.messages().send(data, function (error, body) {
            console.log(body);
          });

          return res.json({
            status: 'SUCCESS',
            jwt: token,
            profile: Object.assign({
              id,
              username: login,
              email,
              timecreated,
              timemodified,
              status,
              chars: [],
            }),
            errors: {},
          });
        } else {
          return res.json({
            status: 'ERROR',
            errors: {
              server: 'Username already taken. Try something else.',
            },
          });
        }
      } catch (error) {
        console.log(error);
        return res.json({
          status: 'ERROR',
          errors: {
            server: 'Username already taken. Try something else.',
          },
        });
      }
    }
  } catch (err) {
    return res.json({
      status: 'ERROR',
      errors: { server: 'An unknown error occured. (2)' },
    });
  }
});

router.put('/email', validate, async (req, res) => {
  try {
    const statement = 'UPDATE accounts SET `email` = ? WHERE id = ?;';
    const result = await req.app.locals.loginQuery(statement, [
      req.body.email,
      req.jwt.id,
    ]);
    if (result.affectedRows) {
      const token = await getJWTForAccountId(req.app.locals.query, req.jwt.id);
      res.send(token);
    }
  } catch (error) {
    res.status(401).send();
  }
});

router.put('/password', validate, async (req, res) => {
  try {
    const statement =
      'UPDATE accounts SET `password` = ?, `salt` = ? WHERE id = ?;';

    const salt = generateSalt();
    const passwordHash = generateHash(req.body.password, salt);

    const result = await req.app.locals.loginQuery(statement, [
      passwordHash,
      salt,
      req.jwt.id,
    ]);
    if (result.affectedRows) {
      const token = await getJWTForAccountId(req.app.locals.query, req.jwt.id);
      res.send(token);
    }
  } catch (error) {
    console.log(error);
    res.status(401).send();
  }
});

router.put('/reset-password', async (req, res) => {
  try {
    const salt = generateSalt();
    const passwordHash = generateHash(req.body.password, salt);

    const statement = `
      UPDATE accounts
      INNER JOIN account_modification_codes ON account_modification_codes.account_id = accounts.id
      SET password = ?, salt = ?, reset_password_code = NULL
      WHERE reset_password_code = ?
    ;`;
    const result = await req.app.locals.loginQuery(statement, [
      passwordHash,
      salt,
      req.body.code,
    ]);
    if (result.affectedRows) {
      res.sendStatus(200);
    } else {
      res.sendStatus(401);
    }
  } catch (error) {
    console.log(error);
    res.status(401).send();
  }
});

router.post('/forgot-password', async (req, res) => {
  const resetPasswordCode = uuid();
  try {
    const statement = `
      UPDATE accounts
      INNER JOIN account_modification_codes ON account_modification_codes.account_id = accounts.id
      SET reset_password_code = ?
      WHERE email = ?
    ;`;
    const result = await req.app.locals.loginQuery(statement, [
      resetPasswordCode,
      req.body.email,
    ]);
    if (result.affectedRows) {
      const data = {
        from: `Limitbreak <noreply@${process.env.MAILGUN_DOMAIN}>`,
        to: req.body.email,
        subject: 'Reset Your Password',
        html: `<p>Hi! Here\'s your password reset link: <a href="${process.env.SITE_URL}/reset?code=${resetPasswordCode}">${process.env.SITE_URL}/reset?code=${resetPasswordCode}</a>.</p>`,
      };

      mg.messages().send(data, function (error, body) {
        console.log(body);
      });

      res.sendStatus(200);
    }
  } catch (error) {
    console.log(error);
    res.status(401).send();
  }
});

router.post('/login', async (req, res) => {
  try {
    const { user, pass } = req.body;

    const statement =
      'SELECT * FROM accounts WHERE `username` = ? AND status = 1;';
    const results = await req.app.locals.loginQuery(statement, [user]);

    // Compare hash to password supplied
    const passwordHash = generateHash(pass, results[0].salt);

    // If the hashes are the same, continue
    if (passwordHash === results[0].password) {
      const token = await getJWTForAccountId(
        req.app.locals.loginQuery,
        results[0].id
      );
      res.send(token);
    } else {
      res.status(401).send();
    }
  } catch (error) {
    console.log(error);
    res.status(500).send();
  }
});

module.exports = router;
