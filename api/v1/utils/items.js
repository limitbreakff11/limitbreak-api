const loadItems = async query => {
  try {
    const statement = `SELECT *, b.itemid AS id, b.name AS name,
                IF(a.itemid IS NOT NULL, 1, 0) AS isArmor,
                IF(f.itemid IS NOT NULL, 1, 0) AS isFurnishing,
                IF(l.itemid IS NOT NULL, 1, 0) AS hasLatents,
                IF(m.itemid IS NOT NULL, 1, 0) AS hasMods,
                IF(mp.itemid IS NOT NULL, 1, 0) AS hasPetMods,
                IF(p.itemid IS NOT NULL, 1, 0) AS isPupAttachment,
                IF(u.itemid IS NOT NULL, 1, 0) AS isUsable,
                IF(w.itemid IS NOT NULL, 1, 0) AS isWeapon
            FROM item_basic AS b
            LEFT JOIN item_equipment AS a ON b.itemid  = a.itemid
            LEFT JOIN item_furnishing AS f ON b.itemid = f.itemid
            LEFT JOIN item_latents AS l ON b.itemid = l.itemid
            LEFT JOIN item_mods AS m ON b.itemid = m.itemid
            LEFT JOIN item_mods_pet AS mp ON b.itemid = mp.itemid
            LEFT JOIN item_puppet AS p ON b.itemid = p.itemid
            LEFT JOIN item_usable AS u ON b.itemid = u.itemid
            LEFT JOIN item_weapon AS w ON b.itemid = w.itemid;`;
    return await query(statement);
  } catch (error) {
    console.error('Error while loading items', error);
    return [];
  }
};

const loadItemKeys = async query => {
  try {
    const statement = `SELECT item_basic.itemid, item_basic.name, level, jobs FROM item_basic
            LEFT JOIN item_equipment ON item_basic.itemid = item_equipment.itemid;`;
    const results = await query(statement);
    const map = {};
    results.forEach(
      r =>
        (map[r.itemid] = {
          key: r.name,
          level: r.level,
          jobs: r.jobs,
        })
    );
    return map;
  } catch (error) {
    console.error('Error while loading item keys', error);
    return {};
  }
};

const getRecipeFor = async (query, itemname) => {
  try {
    const statement = `SELECT * FROM synth_recipes AS r
            JOIN item_basic AS b ON r.result = b.itemid OR resultHQ1 = b.itemid OR resultHQ2 = b.itemid OR resultHQ3 = b.itemid
            WHERE b.name = ?;`;
    return await query(statement, [itemname]);
  } catch (error) {
    console.error('Error while getting specific recipe', error);
    return [];
  }
};

const getLastSold = async (query, itemname, stack = 0, count = 10) => {
  try {
    const statement = `SELECT name, seller_name, buyer_name, sale, sell_date FROM auction_house
            JOIN item_basic on item_basic.itemid = auction_house.itemid
            WHERE sell_date != 0 AND item_basic.name = ? AND stack = ?
            ORDER BY sell_date DESC LIMIT ?;`;
    return await query(statement, [itemname, stack, count]);
  } catch (error) {
    console.error('Error while getting last sold', error);
    return [];
  }
};

const searchLastSold = async (query, itemname, stack = 0, count = 10) => {
  try {
    const statement = `SELECT name, seller_name, buyer_name, sale, sell_date FROM auction_house
            JOIN item_basic on item_basic.itemid = auction_house.itemid
            WHERE sell_date != 0 AND REPLACE(CONCAT(UPPER(LEFT(item_basic.name, 1)), SUBSTRING(item_basic.name, 2)), '_', ' ') LIKE ? AND stack = ?
            ORDER BY sell_date DESC LIMIT ?;`;
    return await query(statement, [`%${itemname}%`, stack, count]);
  } catch (error) {
    console.error('Error while getting last sold', error);
    return [];
  }
};

const getBazaars = async (query, itemname, limit = 300) => {
  try {
    const statement = `SELECT charname, bazaar, IF(s.charid IS NULL, 0, 1) AS online_flag FROM char_inventory AS i
            JOIN item_basic AS b ON b.itemid = i.itemid
            JOIN chars AS c ON c.charid = i.charid
            LEFT JOIN accounts_sessions s on c.charid = s.charid
            WHERE bazaar != 0 AND b.name = ? ORDER BY CASE WHEN online_flag = 1 THEN 1 ELSE 2 END, bazaar, charname ASC LIMIT ?;`;
    return await query(statement, [itemname, limit]);
  } catch (error) {
    console.error('Error while getting bazaars', error);
    return [];
  }
};

const getAllBazaar = async (query, limit = 1000) => {
  try {
    const statement = `SELECT charname, b.name, bazaar, i.itemid as itemid, z.name as zone, IF(s.charid IS NULL, 0, 1) AS online_flag FROM char_inventory AS i
            JOIN item_basic AS b ON b.itemid = i.itemid
            JOIN chars AS c ON c.charid = i.charid
            JOIN zone_settings AS z ON c.pos_zone = z.zoneid
            LEFT JOIN accounts_sessions s on c.charid = s.charid
            WHERE bazaar != 0 AND s.charid IS NOT NULL ORDER BY CASE WHEN online_flag = 1 THEN 1 ELSE 2 END, bazaar, charname ASC LIMIT ?;`;
    return await query(statement, [limit]);
  } catch (error) {
    console.error('Error while getting bazaars', error);
    return [];
  }
};

const searchBazaar = async (query, itemname, limit = 300) => {
  try {
    const statement = `SELECT charname, b.name, bazaar, IF(s.charid IS NULL, 0, 1) AS online_flag FROM char_inventory AS i
            JOIN item_basic AS b ON b.itemid = i.itemid
            JOIN chars AS c ON c.charid = i.charid
            LEFT JOIN accounts_sessions s on c.charid = s.charid
            WHERE bazaar != 0 AND b.name LIKE ? ORDER BY CASE WHEN online_flag = 1 THEN 1 ELSE 2 END, bazaar, charname ASC LIMIT ?;`;
    return await query(statement, [`%${itemname}%`, limit]);
  } catch (error) {
    console.error('Error while getting bazaars', error);
    return [];
  }
};

const getOwners = async (query, itemid) => {
  // Prevent any funny business
  if (!owner.owner_item_list.includes(itemid)) {
    return [];
  }
  try {
    return ownersCache[itemid];
  } catch (error) {
    console.error('Error while getting item owners', error);
    return [];
  }
};

const getJobs = (level, jobs, idToStr) => {
  if (level && jobs) {
    const vals = [];
    for (let job = 0; job < 22; job++) {
      if ((jobs & Math.pow(2, job)) !== 0) {
        vals.push(idToStr[job + 1]);
      }
    }
    return `Lv ${level} ${vals.join(' ')}`;
  } else {
    return null;
  }
};

export {
  loadItems,
  loadItemKeys,
  getRecipeFor,
  getLastSold,
  searchLastSold,
  getBazaars,
  getAllBazaar,
  searchBazaar,
  getOwners,
  getJobs,
};
