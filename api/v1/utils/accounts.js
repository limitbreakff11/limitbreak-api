var sjcl = require('sjcl');
const jwt = require('jsonwebtoken');
const crypto = require('crypto');

const uuid = () => {
  let uuid = '',
    i,
    random;
  for (i = 0; i < 32; i++) {
    random = (Math.random() * 16) | 0;

    if (i == 8 || i == 12 || i == 16 || i == 20) {
      uuid += '-';
    }
    uuid += (i == 12 ? 4 : i == 16 ? (random & 3) | 8 : random).toString(16);
  }
  return uuid;
};

const generateSalt = () => {
  return crypto.randomBytes(16).toString('hex');
};

const generateHash = (password, salt) => {
  const hexedSalt = Buffer.from(salt, 'utf-8').toString('hex');

  var saltBits = sjcl.codec.hex.toBits(hexedSalt);
  const derivedKeyBitArray = sjcl.misc.pbkdf2(password, saltBits, 2048, 32 * 8);
  const passwordHash = sjcl.codec.hex.fromBits(derivedKeyBitArray);

  return passwordHash.toUpperCase();
};

const getJWTForAccountId = async (query, accid) => {
  try {
    const statement = 'SELECT * FROM accounts WHERE `id` = ?;';
    const results = await query(statement, [accid]);
    if (results.length === 1) {
      const {
        id,
        username,
        email,
        timecreated,
        timemodified,
        status,
      } = results[0];
      const token = {
        id,
        username,
        email,
        timecreated,
        timemodified,
        status,
        iss: process.env.SITE_URL,
      };
      return jwt.sign(token, process.env.JWT_SECRET, { expiresIn: '1h' });
    } else {
      return null;
    }
  } catch (error) {
    throw error;
  }
};

module.exports = {
  uuid,
  generateSalt,
  generateHash,
  getJWTForAccountId,
};
