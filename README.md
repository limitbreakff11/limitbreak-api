## Installation & Configuration

### Installation of external dependencies

To start using this project, install [nodejs](https://nodejs.org/en/download/)
Pick the LTS version and go through the installation process with the windows installer

After that, open your command prompt and run the command

```bash
npm install -g yarn
```

Now you can go into the project folder and open the command prompt in that folder.

Run the command

```bash
yarn install
```

## Setup

Copy the `example.env` as `.env` in the root directory and fill in the appropriate credentials.

## Running

## Scripts

Within the scripts folder, there are three files.
You can use these files instead of running the commands below.

```
scripts/start-dev.bat
This one starts the development environment.
```

```
scripts/build-prod.bat
This one builds the project.
```

```
scripts/start-prod.bat
This one starts the production environment.
```

### Production

1. Build the project by performing `yarn build` in the root directory.
2. Then run `yarn start` from the root directory.

### Development

1. Run `yarn dev` in the root directory.

## License
Original work copyright (c) 2020 Eden Server // Modified work copyright (c) 2021 LimitBreak

Permission to use, copy, modify, and/or distribute this software for any purpose with or without fee is hereby granted, provided that the above copyright notice and this permission notice appear in all copies.

THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.